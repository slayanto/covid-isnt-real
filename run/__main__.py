from run import cli_app
import sys

# USAGE_MSG = "Error: Usage: python3 -m run [client | client_local | server]"
USAGE_MSG = "Error: Usage on Unix: ./__main__ [client | client_local | server]"

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(USAGE_MSG)
    else:
        if sys.argv[1] == "client":
            cli_app.start_client()
        elif sys.argv[1] == "client_local":
            cli_app.start_client(False)
        elif sys.argv[1] == "server":
            cli_app.start_server()
        else:
            print(USAGE_MSG)