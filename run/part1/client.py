from bitstring import BitArray
import socket
import sys
from run.utils.utils import *

# if len(sys.argv) == 1 or sys.argv[1] != "local":
    # HOST = 'attu2.cs.washington.edu'  # The server's hostname or IP address
# else:
    # HOST = '127.0.0.1'  # Standard loopback interface address (localhost); should be the same on attu

# HOST = 'attu2.cs.washington.edu'  # The server's hostname or IP address
# HOST = '127.0.0.1'  # Standard loopback interface address (localhost); should be the same on attu
PORT = 12235  # The port used by the server
USER = '[Client]'
secrets = []
ID = 146


def main(attu=True):
    if attu:
        HOST = 'attu2.cs.washington.edu'  # The server's hostname or IP address
    else:
        HOST = '127.0.0.1'  # Standard loopback interface address (localhost); should be the same on attu
    # part a
    data = part1a(HOST)
    print(USER, "bytes, ", data)
    payload_len, psecret, step, digits, num, length, udp_port, secretA = \
        data.unpack('uintbe:32, uintbe:32, uintbe:16, uintbe:16, uintbe:32, uintbe:32, uintbe:32, uintbe:32')
    print(USER, "Server response of {} bytes: num: {}, len: {}, udp_port: {}, secretA: {}".format(len(data)/8, num, length, udp_port, secretA))
    secrets.append(secretA)
    print(USER, "num, len, udp_port ", num, length, udp_port)

    # part b
    data = part1b(num, length, secretA, udp_port, HOST)
    payload_len, psecret, step, digits, tcp_port, secretB = \
        data.unpack('uintbe:32, uintbe:32, uintbe:16, uintbe:16, uintbe:32, uintbe:32')
    print(USER, "Secret B is", secretB)
    secrets.append(secretB)
    print(USER, "tcp_port", tcp_port)

    # part c and d
    data = part1candd(tcp_port, HOST)
    print(secrets)

# returns data in big endian order
def part1a(HOST):
    payload = b"hello world\0"
    payload_len = len(payload) # each character is a byte
    print(USER, "Sending to server: payload: {}".format(payload))
    packet = build_packet(payload_len, 0, 1, ID, payload)
    # sock_dgram is udp, sock_stream is tcp
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.sendto(packet.tobytes(), (HOST, PORT))
        return BitArray(s.recvfrom(1024)[0])


def part1b(num, length, secret, udp_port, HOST):
    # send num packets of len+4 on udp_port, verifying an ack before sending another
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.settimeout(0.5)
        for i in range(0, num):
            data = BitArray()
            data.append('uintbe:32={}'.format(i))
            print(USER, "data being sent", data)
            pad_bytes(data, length)
            packet = build_packet(length + 4, secret, 1, ID, data)
            print(USER, "packet part b ", packet)

            response = None
            packetId = -1
            while packetId != i:
                print(USER, "sent packet, ", i)
                s.sendto(packet.tobytes(), (HOST, udp_port))
                try:
                    response = s.recvfrom(1024)
                    payload_len, psecret, step, digits, packetId = BitArray(response[0]).unpack(
                        "uintbe:32, uintbe:32, uintbe:16, uintbe:16, uintbe:32")
                    print(USER, "Received, ", packetId)
                except:
                    pass

        return BitArray(s.recvfrom(1024)[0])


def part1candd(port, HOST):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        # part c
        s.connect((HOST, port))
        response = BitArray(s.recv(1024))
        print(USER, "Received part c", response)
        payload_len, psecret, step, digits, num, length, secretC, character = \
            response.unpack('uintbe:32, uintbe:32, uintbe:16, uintbe:16, uintbe:32, uintbe:32, uintbe:32, uintbe:8')
        print(USER, "Secret C is", secretC)
        secrets.append(secretC)
        print(USER, "num, length, character", num, length, character)

        # part d
        for i in range(0, num):
            data = BitArray()
            for i in range(0, length):
                data.append('uintbe:8={}'.format(character))
            packet = build_packet(length, secretC, 1, ID, data)
            print(USER, "packet part d", packet)
            s.sendall(packet.tobytes())
        response = BitArray(s.recv(1024))
        payload_len, psecret, step, digits, secretD = \
            response.unpack('uintbe:32, uintbe:32, uintbe:16, uintbe:16, uintbe:32')
        print(USER, "Secret D is", secretD)
        secrets.append(secretD)


if __name__ == "__main__":
    main()
