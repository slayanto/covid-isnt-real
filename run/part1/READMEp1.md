Python Version 3.7.0
Will Taylor: wjt8
Stefan Layanto: slayanto
Dylan Burton: burtond

[75, 64, 1, 1]

To compile & run:
1) pip install -r requirements.txt
2) navigate to run/part1

To run locally:
3) python3 client.py local

To run against Attu2:
3) python3 client.py attu
