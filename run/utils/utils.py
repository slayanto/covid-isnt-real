from bitstring import BitArray
import socket
import struct

def validate_header(payload_len, psecret, step, last_3_digits_id):
    try:
        assert(type(payload_len))
        assert(type(step) is int and step >= 0)
        assert(type(last_3_digits_id) is int and last_3_digits_id >= 0 and last_3_digits_id < 65536)
    except Exception as e:
        print("Invalid Header: ")
        return_str = "HEADER\nlen:\t{}\nsecret:\t{}\nstep:\t{}\nid:\t{}\n".format(payload_len, psecret, step, last_3_digits_id)
        print(return_str)

# Takes in payload parameters, returns a bitstring (BIG ENDIAN)
# payload_len: 4 byte int, secret: 4 byte bitstring, step: 2 byte int, id: 2 byte int
# Note that psecret is passed in as a bitarray
def build_header(payload_len, psecret, step, last_3_digits_id):
    validate_header(payload_len, psecret, step, last_3_digits_id)
    # return_str = "BUILT HEADER\nlen:\t{}\nsecret:\t{}\nstep:\t{}\nid:\t{}\n".format(payload_len, psecret, step, last_3_digits_id)
    # print(return_str)
    s = BitArray()
    s.append('uintbe:32={}'.format(payload_len)) # payload_len
    s.append('uintbe:32={}'.format(psecret))
    s.append('uintbe:16={}'.format(step)) # step
    s.append('uintbe:16={}'.format(last_3_digits_id)) # id
    return s

# Takes in a bitstring (LITTLE ENDIAN), returns tuple of parameters.
# Note that psecret is returned as a bitarray
def parse_header(header):
    payload_len, psecret, step, last_3_digits_id = header.unpack('uintbe:32, uintbe:32, uintbe:16, uintbe:16')
    return_str = "PARSED HEADER\nlen:\t{}\nsecret:\t{}\nstep:\t{}\nid:\t{}\n".format(payload_len, psecret, step, last_3_digits_id)
    print(return_str)
    validate_header(payload_len, psecret, step, last_3_digits_id)
    return (payload_len, psecret, step, last_3_digits_id)

# temp_secret = BitArray('0xff00ff00') # A 4 byte bitstring
# header = build_header(4, temp_secret, 1, 146)
# print(header)
# parse_header(header)

# build a 4 byte aligned packet with a header, and FLIPS endianness
def build_packet(payload_len, psecret, step, last_3_digits_id, payload):
    packet = build_header(payload_len, psecret, step, last_3_digits_id)
    payload_bits = BitArray(payload)
    payload_bits.byteswap()
    packet.append(payload)
    bytes_to_add = (-1 * payload_len) % 4
    pad_bytes(packet, bytes_to_add)
    # packet.byteswap()
    return packet

# takes a bitarray and appends 0s to it
def pad_bytes(data, num):
    for i in range(0, num):
        data.append('0x00')

print("Stef you baboon you accidentally ran utils again")