from run.part1.client import main
from run.part2.server import run_server

def start_client(attu=True):
    main(attu)

def start_server():
    run_server()