#!/usr/bin/env python3

import socket
import random
import threading, _thread
from bitstring import BitArray
from datetime import datetime
from run.utils.utils import *

HOST = '127.0.0.1'  # Standard loopback interface address (localhost); should be the same on attu
PORT = 12235        # Port to listen on (non-privileged ports are > 1023)
LARGESTPORTVAL = 65535 # to be used as a max value in random generation
MAXLEN = 100
HEADERLEN = 12 
SID = 514
secrets = []

def handle_b(num, length, udp_port, secretA):
    print("Handling B on port " + str(udp_port) + " for args:\t", sep="")
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.bind((HOST, udp_port))
        print("[Server] Stage B: listening at " + str(HOST) +  " on port " + str(udp_port) + "...")
        currNum = 0
        lastPack = datetime.utcnow()
        while currNum < num and (datetime.utcnow() - lastPack).total_seconds() < 3:
            data, addr = s.recvfrom(1024)
            if data:
                # verify header & body, verify ordering
                bitArrData = BitArray(bytes=data)
                payload_len, psecret, step, last_3_digits_id, packNum = bitArrData.unpack('uintbe:32, uintbe:32, uintbe:16, uintbe:16, uintbe:32')
                try:
                    validate_header(payload_len, psecret, step, last_3_digits_id)
                    assert(psecret == secretA)
                    assert(payload_len == length + 4)
                    assert(packNum == currNum)
                except Exception as e:
                    print("[Server] Stage B: Invalid Header or content; exiting with exception: " + str(e))
                    # invalid: return
                    return
                # valid: send an ack if bool says to, else nothing
                willSendAck = random.randint(0, 1)
                if bool(willSendAck):
                   print("[Server] Stage B: Sending Ack For Packet Num " + str(currNum)) 
                   bitArr = BitArray()
                   bitArr.append('uintbe:32={}'.format(currNum))
                   s.sendto(build_packet(4, secretA, 2, SID, bitArr).tobytes(), addr)
                   currNum = currNum + 1
                lastPack = datetime.utcnow()
        if currNum == num:
            tcp_port = rand_port(LARGESTPORTVAL)
            secretB = random_len(MAXLEN)
            secrets.append(secretB)
            bitArr = BitArray()
            bitArr.append('uintbe:32={}'.format(tcp_port))
            bitArr.append('uintbe:32={}'.format(secretB))
            s.sendto(build_packet(8, secretA, 2, SID, bitArr).tobytes(), addr)
            _thread.start_new_thread(handle_c_and_d, (tcp_port, secretB))
        return

def handle_c_and_d(tcp_port, secretB):
    print("Handling C/D on port " + str(tcp_port) + " for args:\t", sep="")
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, tcp_port))
        s.listen()
        print("[Server] Stage C/D: Listening at " + str(HOST) +  " on port " + str(tcp_port) + "...")
        conn, addr = s.accept()
        with conn:
            print("[Server] Stage C/D: Connected to client at " + str(addr))
            num2 = random_len(MAXLEN)
            len2 = random_len(MAXLEN)
            secretC = random_len(MAXLEN)
            secrets.append(secretC)
            char = b'c'
            bitArr = BitArray()
            bitArr.append('uintbe:32={}'.format(num2))
            bitArr.append('uintbe:32={}'.format(len2))
            bitArr.append('uintbe:32={}'.format(secretC))
            bitArr.append(char)
            conn.sendall(build_packet(13, secretB, 2, SID, bitArr).tobytes())
            currNum = 0
            lastPack = datetime.utcnow()
            while currNum < num2 and (datetime.utcnow() - lastPack).total_seconds() < 3:
                # data = conn.recv(HEADERLEN + len2) # packet size = HEADERLEN + len2
                data = conn.recv(HEADERLEN + len2 + (4 - (len2 % 4))) # packet size = HEADERLEN + len2 padded to 4 bytes
                if data:
                    # verify header & body, verify ordering
                    bitArrData = BitArray(bytes=data)
                    payload_len, psecret, step, last_3_digits_id = parse_header(bitArrData)
                    try:
                        validate_header(payload_len, psecret, step, last_3_digits_id)
                        assert(psecret == secretC)
                        assert(payload_len == len2)
                        offset = payload_len*8
                        unpacked = bitArrData[-offset:].unpack('uintbe:8')
                        for i in range(len(unpacked)):
                            assert(chr(unpacked[i]) == 'c')
                    except Exception as e:
                        print("[Server] Stage C/D: Invalid Header or content; exiting with exception: " + str(e))
                        # invalid: return
                        return
                    currNum = currNum + 1
                    lastPack = datetime.utcnow()
            if currNum == num2:
                secretD = random_len(MAXLEN)
                secrets.append(secretD)
                bitArr = BitArray()
                bitArr.append('uintbe:32={}'.format(secretD))
                conn.sendall(build_packet(4, secretC, 2, SID, bitArr).tobytes())
                print("[Server] Final: Successful on steps A-D with secrets generated:")
                print(secrets)
                print("[Server Final: Closing TCP connection")
            return

# Constantly run 
def run_server():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s: 
        s.bind((HOST, PORT))
        print("[Server] Stage A: Listening at " + str(HOST) +  " on port " + str(PORT) + "...")
        while True: # so entry loops forever
            while True:
                data, addr = s.recvfrom(1024)
                bitArr = BitArray(bytes=data)
                print("[Server] Stage A: Recieved "  + str(data) + " from: " + str(addr))
                # validate header
                try:
                    payload_len, psecret, step, last_3_digits_id = parse_header(bitArr)
                    print("[Server] Stage A: Initial Header Contents: ", payload_len, psecret, step, last_3_digits_id)
                    validate_header(payload_len, psecret, step, last_3_digits_id)
                    assert(psecret == 0) # initialized as such in spec for A
                    assert(bitArr[-payload_len*8:] == b"hello world\0")
                except Exception as e:
                    print("[Server] Stage A: Invalid Header or content; continuing with caught exception: " + str(e))
                    break
                
                num, length, udp_port, secretA = random_len(MAXLEN), random_len(MAXLEN), rand_port(LARGESTPORTVAL), random_len(MAXLEN)
                secrets.append(secretA)
                bitArr = BitArray()
                bitArr.append('uintbe:32={}'.format(num)) 
                bitArr.append('uintbe:32={}'.format(length))
                bitArr.append('uintbe:32={}'.format(udp_port))
                bitArr.append('uintbe:32={}'.format(secretA)) 
                s.sendto(build_packet(HEADERLEN, 0, 2, SID, bitArr).tobytes(), addr) 

                _thread.start_new_thread(handle_b, (num, length, udp_port, secretA))

def random_len(max):
    return random.randint(1, max)

def rand_port(max):
    return random.randint(8000, max)


if __name__ == "__main__":
    run_server()